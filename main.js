let username = ''; //On définit une variable vide pour le nom de l'utilisateur

let userQuestion = 'Question d\'exemple'; //Cette variable contiendra la question de l'utilisateur
console.log(userQuestion);

let randomNumber = Math.floor(Math.random() * 8 ); //On définit une variable qui correspond à un chiffre aléatoire 
let eightBall = ''; //On définit une variable vide qui contiendra la réponse
switch(randomNumber){ //On définit un switch sur la valeur de la variable RandomNumber
    case 0 : //Pour le cas n° 0
        eightBall = 'Peut être'; //La variable eightBall prend la valeur
    break;
    case 1 :
        eightBall = 'Oui sûrement';
    break;   
    case 2 :
        eightBall = 'Très probablement';
    break;
    case 3 :
        eightBall = 'Très sûrement';
    break;
    case 4 :
        eightBall = 'Bof';
    break;
    case 5 :
        eightBall = 'A revoir';
    break;
    case 6 :
        eightBall = 'Sûrement pas';
    break;
    case 7 :
        eightBall = 'Probablement que non';
    break;
    case 8 :
        eightBall = 'Vraiment pas';
    break;
    default :
        eightBall = 'Pas de réponse';
    break;
}

console.log(eightBall); //On affiche la réponse 
/*
    Afficher pour l'utilisateur les résultats
*/

const form = document.getElementById('form_magic') //On récupère le formulaire
const recapQuestionBlock = document.getElementById('recapQuestion'); //On récupère la div qui va permettre d'afficher la réponse donnée à l'utilisateur
const responseBlock = document.getElementById('response'); //On récupère la div qui va permettre d'afficher la réponse donnée à l'utilisateur

//On veut une fonction qui s'active au clic sur le bouton submit
function submitForm(event){
    event.preventDefault(); //On empêche que l'évènement par défaut se réalise

    //On affiche un message de salutation à l'utilisateur
    username = form.nomUtilisateur.value;
    username ? recapQuestionBlock.append(`Bonjour ${username}, `) : recapQuestionBlock.append('Bonjour! '); //Avec une ternaire on vérifie s'il y a un nom d'utilisateur renseigné sinon on met juste bonjour
    
    //On affiche la question et son résultat
    userQuestion = form.question.value; //la variable userQuestion va prendre la valeur du formulaire

    recapQuestionBlock.append(`ta question était : ${userQuestion} ?`); //On ajoute dans la div le texte suivant + la question de l'utilisateur
    responseBlock.append(eightBall);
    responseBlock.style.fontWeight = 'bold';
}
//On demande au formulaire d'appliquer la fonction 
form.addEventListener('submit', submitForm);

//L'utilisateur doit pouvoir rentrer sa question

//La réponse s'affichera en dessous 